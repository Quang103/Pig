import unittest
from Dice import Dice
from Dice import RolledOneException
from contextlib import suppress

class TestDiceClass(unittest.TestCase):

    def test_init_default_object(self):
        new_Dice = Dice()
        self.assertIsInstance(new_Dice, Dice)

    def test_roll(self):
        new_Dice = Dice()
        roll = new_Dice.roll()
        ifroll = (1 <= roll <= 6)
        self.assertTrue(ifroll)

    
    def test_random(self):
        new_Dice = Dice()
        numbers = []
        i = 0
        with suppress(RolledOneException):
            numbers.append(new_Dice.roll())
            while i < 10:
                i = i + 1
                numbers.append(new_Dice.roll())
                if numbers[i] != numbers[i-1]:
                    break
                
        
        self.assertNotEqual(numbers[i],numbers[i-1])

if __name__=="__main__":
    unittest.main()
