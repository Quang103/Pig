import unittest
import EasyCom
class TestEasyComClass(unittest.TestCase):
    def test_init_default_object(self):
        new_comp = EasyCom.EasyComputerPlayer()
        self.assertIsInstance(new_comp,EasyCom.EasyComputerPlayer)
    
if __name__=="__main__":
    unittest.main()