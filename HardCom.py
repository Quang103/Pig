import random
from Player import P_Player
class HardcomputerPlayer(P_Player):
    """Randomly decides if the CPU player will keep rolling."""

    def __init__(self, name):
        super(HardcomputerPlayer,self).__init__(name)
    
    def roll_again(self,cage):
        """Randomly decides if the HardCPU player will keep rolling."""

        while cage.value < (6 + random.randint(20,40)):
            print( "Role again")
            return True
        print ("CPU will hold")
        return False