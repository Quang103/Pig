from Player import P_Player
class Human(P_Player):
    def __init__(self, name):
        super(Human,self).__init__(name)
    
    def roll_again(self,box):
        """Asks the human player, if they want to keep rolling."""
        while True:
            human_choice = int(input("Choice 1 to roll again and 0 to hold" ))
            if human_choice == 1:
                return True
            if human_choice == 2:
                self.access_menu()
            else:
                return False

    def access_menu(self):
        print("""Player menu: 
                - Press 1 to cheat
                - Press 2 to quit the game
                - Press 3 to resume the game""")
        while True:
            menu_choice = int(input(" "))
            if menu_choice == 1:
                point_add = int(input("How much score do you want to gain "))
                self.plus_score(point_add)
            if menu_choice == 2:
                new_name = str(input("Change your username "))
                self.set_name(new_name)
            if menu_choice == 3:
                quit()
            if menu_choice == 4:
                return False
