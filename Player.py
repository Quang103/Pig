class P_Player(object):
    def __init__(self, name = None):
        self.name = name
        self.score = 0
    
    def plus_score(self,player_score):
        self.score += player_score

    def set_name(self,name):
        self.name = name

    def __str__(self):
        return str(self.name) + ": " + str(self.score)